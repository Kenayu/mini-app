﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using TMPro;

public class BubbleSpeech : MonoBehaviour
{
    public TextMeshProUGUI nameDisplay;
    public TextMeshProUGUI emailDisplay;

    private void Start()
    {
        WWW att = new WWW("https://5e510330f2c0d300147c034c.mockapi.io/users");
        StartCoroutine(ProcessAttachment(att));
    }

    private IEnumerator ProcessAttachment(WWW attachment)
    {
        yield return attachment;

        nameDisplay.text = AttachName(attachment.text);
        emailDisplay.text = AttachEmail(attachment.text);
    }

    private string AttachName(string json)
    {
        JSONArray arr = JSON.Parse(json).AsArray;

        return arr[3]["name"].Value;
    }

    private string AttachEmail(string json)
    {
        JSONArray arr = JSON.Parse(json).AsArray;

        return arr[3]["email"].Value;
    }

    public class Humane
    {
        public string name;
        public string email;

        public Humane(string name, string email)
        {
            this.name = name;
            this.email = email;
        }
    }
}
