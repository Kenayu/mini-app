## Bubble Speech Mini App
<!-- blank line -->
<br>
<!-- blank line -->
Nama : Niken Ayu Anggarini

NRP  : 4210181003
<!-- blank line -->
**Tentang Project**

Project ini berisikan sebuah program yang menampilkan nama dan email, yang diambil dari dari [link JSON ini](https://5e510330f2c0d300147c034c.mockapi.io/users). Adapun cara kerja kode yang ada di dalamnya adalah sebagai berikut.

Mendownload data [link JSON ini](https://5e510330f2c0d300147c034c.mockapi.io/users)
```
    private void Start()
    {
        WWW att = new WWW("https://5e510330f2c0d300147c034c.mockapi.io/users");
        StartCoroutine(ProcessAttachment(att));
    }

    private IEnumerator ProcessAttachment(WWW attachment)
    {
        yield return attachment;

        nameDisplay.text = AttachName(attachment.text);
        emailDisplay.text = AttachEmail(attachment.text);
    }
```
Mengambil data berupa nama, "name" dari array ke-3
```
    private string AttachName(string json)
    {
        JSONArray arr = JSON.Parse(json).AsArray;

        return arr[3]["name"].Value;
    }
```
Mengambil data berupa email dari array ke-3
```
    private string AttachEmail(string json)
    {
        JSONArray arr = JSON.Parse(json).AsArray;

        return arr[3]["email"].Value;
    }
```
Output dari program ini adalah sebagai berikut.
<!-- blank line -->
![](MiniApp.gif)
